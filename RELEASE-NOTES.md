# 0.4.1

- Add an `#[inline]`, increasing SSSE3 decode throughput significantly
  ([PR](https://bitbucket.org/marshallpierce/stream-vbyte-rust/pull-requests/8))
- Bump MSRV to 1.64.0

# 0.4.0

- User-provided decode sinks
- Rearrange modules
- Rust edition 2018

# 0.3.1, 0.3.2

- Uninteresting build system changes

# 0.3.0

- `DecodeCursor` for efficiently navigating very large encoded datasets
- x86 SSE4.1 encoder

# 0.2.0

- x86 SSSE3 decoder
- Follow updated spec for byte ordering

# 0.1.0

- Initial release
