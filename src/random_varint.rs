use rand::{
    distributions::{Distribution as _, Uniform},
    Rng,
};

// Evenly distributed random numbers end up biased heavily towards longer encoded byte lengths:
// there are a lot more large numbers than there are small (duh), but for exercising serialization
// code paths, we'd like many at all byte lengths. This is also arguably more representative of
// real data. This should emit values whose varint lengths are uniformly distributed across the
// whole length range.
pub struct RandomVarintEncodedLengthIter<R: Rng> {
    ranges: [Uniform<u32>; 4],
    range_for_picking_range: Uniform<usize>,
    rng: R,
}

impl<R: Rng> RandomVarintEncodedLengthIter<R> {
    pub fn new(rng: R) -> RandomVarintEncodedLengthIter<R> {
        RandomVarintEncodedLengthIter {
            ranges: [
                Uniform::new(0, 1 << 8),
                Uniform::new(1 << 8, 1 << 16),
                Uniform::new(1 << 16, 1 << 24),
                Uniform::new(1 << 24, u32::max_value()), // this won't ever emit the max value, sadly
            ],
            range_for_picking_range: Uniform::new(0, 4),
            rng,
        }
    }
}

impl<R: Rng> Iterator for RandomVarintEncodedLengthIter<R> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        // pick the range we'll use
        let value_range = self.ranges[self.range_for_picking_range.sample(&mut self.rng)];

        Some(value_range.sample(&mut self.rng))
    }
}
